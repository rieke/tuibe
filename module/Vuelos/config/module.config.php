
<?php
return array(
    
    'router' => array(
        
        'routes' => array(
            
            'hello' => array(
                
                'type' => 'Literal',
                
                'options' => array(
                    
                    'route' => '/vuelos',
                    
                    'defaults' => array(
                        
                        'controller' => 'Vuelos\Controller\Vuelos',
                        
                        'action' => 'index'
                    
                    )
                
                ),
                
                'may_terminate' => true,
                
                'child_routes' => array(
                    
                    'default' => array(
                        
                        'type' => 'Segment',
                        
                        'options' => array(
                            
                            'route' => '/[:action[/:id]]',
                            
                            'constraints' => array(
                                
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            
                            )
                        
                        )
                    
                    )
                
                )
            
            )
        
        )
    
    ),
    
    'controllers' => array(
        
        'invokables' => array(
            
            'Vuelos\Controller\Vuelos' => 'Vuelos\Controller\VuelosController'
        
        )
    
    ),
    
    'view_manager' => array(
        
        'template_path_stack' => array(
            
            __DIR__ . '/../view'
        
        )
    
    )

);
