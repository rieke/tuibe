<?php

namespace Vuelos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Json;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Json\Decoder;
use Vuelos\Form\VuelosForm;
use Vuelos\Model\EntidadVuelos;
use Vuelos\Model\EntidadMostrarVuelos;
use Zend\Session\Container;


class VuelosController extends AbstractActionController

{
    /*
     * Action para la pantalla de busqueda de vuelos
     */
    public function indexAction()
    
    {
        //creo el formulario para la pantalla de busqueda, y le añado los datos para el selector de salidas
        $form=new VuelosForm('reservas');
        $departures=$this->cargarDepartures();
        $form->cargarDeparture($departures);
        return new ViewModel(array(
            'form' => $form,
            
        ));
        
    }
    
    /*
     * Action para la pantalla de seleccion de vuelos
     */
    public function mostrarrutasAction(){
        
        $post=$this->request->getPost();
        $entidadVuelos=new EntidadVuelos($post);
        //cargo los vuelos de salida con los datos del formulario
        $vuelosSalida=$this->cargarvuelos($entidadVuelos,false);
        //cargo los datos de vuelta con los datos del formulario
        $vuelosVuelta=$this->cargarvuelos($entidadVuelos,true);
        
        //guardo la informacion de los vuelos, para saber cual elige el usuario
        $userContainer = new Container('vuelos');
        $userContainer->offsetSet("salida", $vuelosSalida);
        $userContainer->offsetSet("vuelta", $vuelosVuelta);
        
        return new ViewModel(array('salida' => $vuelosSalida,'vuelta'=>$vuelosVuelta));
    }
    
    /*
     * Action para la pantalla de resumen
     */
    public function resumenAction(){
        //recupero la informacion de los vuelos guardados en la session
        $userContainer = new Container('vuelos');
        $salida=$userContainer->offsetGet("salida");
        $llegada=$userContainer->offsetGet("vuelta");
        
        //con la seleccion del usuario, puedo saber que vuelo ha elegido, ya que guardo el indice que el usuario ha seleccionado dentro del array de vuelos
        $post=$this->request->getPost();
        $indiceSalida=$post['salida'];
        $indiceVuelta=$post['vuelta'];
        
        return new ViewModel(array('salida' => $salida[$indiceSalida],'vuelta'=>$llegada[$indiceVuelta]));
    }
    
    //METODOS AUXILIARES, NO SE UTILIZAN PARA CARGAR VISTAS
    
    /*
     * Utiliza la API para recuperar los aeropuertos de salida
     */
    public function cargarDepartures(){
        $request = new Request();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        $request->setUri('http://tui-applicant-test.duckdns.org/api/json/1F/flightroutes');
        //$request->setMethod('POST');
        
        //$request->setPost(new Parameters(array('someparam' => $somevalue)));
        
        $client = new Client();
        $client->setAuth('phpdev','xgWT2=2c', \Zend\Http\Client::AUTH_BASIC);
        $response = $client->dispatch($request);
        $data = json_decode($response->getBody(), true);
        
        $data2=Decoder::decode($response->getBody());
        
        $rutas=$data2->flightroutes;
        $salidas=array();
        foreach ($rutas as $ruta){
            $depCode=$ruta->DepCode;
            $depName=$ruta->DepName;
            
            $salidas[$depCode]=$depName;
        }
        
        return $salidas;
    }
    
    /*
     * Utiliza la API para recuperar los aeropuertos de destino
     * Metodo para utilizar desde AJAX
     */
    public function cargardestinationAction(){
        $departure="OST2";
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {
                //var_dump($this->getRequest()->getPost('departure'));
                $departure= $this->getRequest()->getPost('departure');
            }
        }
        else {
            echo 'Not Ajax';
            // ... Do normal controller logic here (To catch non ajax calls to the script)
        }
        
        $request = new Request();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        $request->setUri('http://tui-applicant-test.duckdns.org/api/json/1F/flightroutes/?locale=en_GB&departureairport='.$departure);
        //$request->setMethod('POST');
        
        //$request->setPost(new Parameters(array('someparam' => $somevalue)));
        
        $client = new Client();
        $client->setAuth('phpdev','xgWT2=2c', \Zend\Http\Client::AUTH_BASIC);
        $response = $client->dispatch($request);
        $data = json_decode($response->getBody(), true);
        
        $data2=Decoder::decode($response->getBody());
        
        $rutas=$data2->flightroutes;
        $salidas=array();
        foreach ($rutas as $ruta){
            $depCode=$ruta->RetCode;
            $depName=$ruta->RetName;
            
            $salidas[$depCode]=$depName;
        }
        
        $result = new JsonModel($salidas);
        
        return $result;
    }
    
    /*
     * Utiliza la API para recuperar los fechas de salida desde un aeropuerto a otro
     * Metodo para utilizar desde AJAX
     */
    public function cargarfechasAction(){
        $departure="OST2";
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {
                //var_dump($this->getRequest()->getPost('departure'));
                $departure= $this->getRequest()->getPost('departure');
                $destination= $this->getRequest()->getPost('destination');
            }
        }
        else {
            echo 'Not Ajax';
            // ... Do normal controller logic here (To catch non ajax calls to the script)
        }
        
        $request = new Request();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        $request->setUri('http://tui-applicant-test.duckdns.org/api/json/1F/flightschedules?departureairport='.$departure.'&destinationairport='.$destination.'&locale=en_GB&returndepartureairport='.$destination.'&returndestiantionairport='.$departure);
        //$request->setMethod('POST');
        
        //$request->setPost(new Parameters(array('someparam' => $somevalue)));
        
        $client = new Client();
        $client->setAuth('phpdev','xgWT2=2c', \Zend\Http\Client::AUTH_BASIC);
        $response = $client->dispatch($request);
        $data = json_decode($response->getBody(), true);
        
        $data2=Decoder::decode($response->getBody());
        
        $fSalidas=$data2->flightschedules->OUT;
        $salidas=array();
        foreach ($fSalidas as $fSalida){
            $salidas[]=$fSalida->date;
        }
        
        $fLlegadas=$data2->flightschedules->RET;
        $llegadas=array();
        foreach ($fLlegadas as $fLlegada){
            $llegadas[]=$fLlegada->date;
        }
        
        $result = new JsonModel(array('salidas'=>$salidas,'llegadas'=>$llegadas));
        
        return $result;
    }
    
    /*
     * Utiliza la API para recuperar los vuelos posibles con la informacion seleccionada por el usuario
     */
    public function cargarvuelos($entidadVuelos,$return){
        
        
        $request = new Request();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        if ($return){
            $stringURL='http://tui-applicant-test.duckdns.org/api/json/1F/flightavailability/?locale=en_GB&departureairport='.$entidadVuelos->getDestination().'&destinationairport='.$entidadVuelos->getDeparture().'&departuredate='.$entidadVuelos->getFvuelta().'&adults='.$entidadVuelos->getAdultos().'&children='.$entidadVuelos->getChildren().'&infants='.$entidadVuelos->getBabies();
        }else{
            $stringURL='http://tui-applicant-test.duckdns.org/api/json/1F/flightavailability/?locale=en_GB&departureairport='.$entidadVuelos->getDeparture().'&destinationairport='.$entidadVuelos->getDestination().'&departuredate='.$entidadVuelos->getFsalida().'&adults='.$entidadVuelos->getAdultos().'&children='.$entidadVuelos->getChildren().'&infants='.$entidadVuelos->getBabies();
        }

        $request->setUri($stringURL);
        
        $client = new Client();
        $client->setAuth('phpdev','xgWT2=2c', \Zend\Http\Client::AUTH_BASIC);
        $response = $client->dispatch($request);
        $data = json_decode($response->getBody(), true);
        $data2=Decoder::decode($response->getBody());
        
        $salidas=$data2->flights->OUT;
        $listaVuelos=array();
        $indice=0;
        $indicePrecioMinimo=0;
        $precioMinimo=99999;
        foreach ($salidas as $salida){
            $mostrarVuelo=new EntidadMostrarVuelos($salida);
            $mostrarVuelo->setAdults($entidadVuelos->getAdultos());
            $mostrarVuelo->setChildren($entidadVuelos->getChildren());
            $listaVuelos[]=$mostrarVuelo;
            if ($mostrarVuelo->getPrice()<$precioMinimo){
                $precioMinimo=$mostrarVuelo->getPrice();
                $indicePrecioMinimo=$indice;
            }
            $indice++;
        }
        
        //ya tenemos el indice del objeto con precio minimo, asi que lo marcamos
        $listaVuelos[$indicePrecioMinimo]->setEsPrecioMinimo(true);
        
        
        return $listaVuelos;
    }
    
}