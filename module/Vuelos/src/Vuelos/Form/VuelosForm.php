<?php


namespace Vuelos\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class VuelosForm extends Form{
    
    public function __construct($name = 'Página principal')
    {
        parent::__construct($name);
        $this->setAttribute("onsubmit", "return verify()");
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'destination',
            'attributes' => array(
                'onchange'=>'destination_changed(this)',
                'id' => 'destination'
            ),
            'options' => array(
                'label' => 'Salida',
                'empty_option' => 'To',
                'value_options' => array(),
            )
        ));
        
        $fechaSalida = new Element\Text('fsalida');
        $this->add($fechaSalida);
        
        $fechaVuelta = new Element\Text('fvuelta');
        $this->add($fechaVuelta);
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'adultos',
            'options' => array(
                'label' => 'Salida',
                'value_options' => array(
                    '1' => '1 Adulto',
                    '2' => '2 Adulto',
                    '3' => '3 Adulto',
                    '4' => '4 Adulto',
                    '5' => '5 Adulto',
                    '6' => '6 Adulto',
                    '7' => '7 Adulto',
                    '8' => '8 Adulto',
                    '9' => '9 Adulto',
                    
                ),
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'children',
            'options' => array(
                'label' => 'Salida',
                'value_options' => array(
                    '0' => '0 Children',
                    '1' => '1 Children',
                    '2' => '2 Children',
                    '3' => '3 Children',
                    '4' => '4 Children',
                    '5' => '5 Children',
                    '6' => '6 Children',
                    '7' => '7 Children',
                    '8' => '8 Children',
                    '9' => '9 Children',
                    
                ),
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'babies',
            'options' => array(
                'label' => 'Salida',
                'value_options' => array(
                    '0' => '0 Babies',
                    '1' => '1 Baby',
                    '2' => '2 Babies',
                    '3' => '3 Babies',
                    '4' => '4 Babies',
                    '5' => '5 Babies',
                    '6' => '6 Babies',
                    '7' => '7 Babies',
                    '8' => '8 Babies',
                    '9' => '9 Babies',
                    
                ),
            )
        ));
        
        $submit = new Element\Submit('submit');
        $submit->setValue('Search and book');
        $submit->setAttribute("class", "botonSubmit");
        $this->add($submit);
    }
    
    public function cargarDeparture($listaDeparture){
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'departure',
            'attributes' =>array(
                'onchange'=>'departure_changed(this)',
                'id'=>'departure'
            ),
            'options' => array(
                'label' => 'Salida',
                'empty_option' => 'From',
                'value_options' => $listaDeparture,
            )
        ));
    }
    
}