<?php

namespace Vuelos\Model;

class EntidadMostrarVuelos{
    private $date;
    private $aircrafttype;
    private $datetime;
    private $price;
    private $seatsAvailable;
    private $departCode;
    private $departName;
    private $arrivalCode;
    private $arrivalName;
    
    private $esPrecioMinimo;
    private $adults;
    private $children;
    
    public function __construct($data){
        $this->date=$data->date;
        $this->aircrafttype=$data->aircrafttype;
        $this->datetime=$data->datetime;
        $this->price=$data->price;
        $this->seatsAvailable=$data->seatsAvailable;
        
        $this->departCode=$data->depart->airport->code;
        $this->departName=$data->depart->airport->name;
        $this->arrivalCode=$data->arrival->airport->code;
        $this->arrivalName=$data->arrival->airport->name;
        $this->esPrecioMinimo=false;
        $this->adults=0;
        $this->children=0;
    }
    /**
     * @return the $date
     */
    public function getDate()
    {
        $dateFormat=explode("-", $this->date);
        return $dateFormat[2]."/".$dateFormat[1].'/'.$dateFormat[0];
    }

    /**
     * @return the $aircrafttype
     */
    public function getAircrafttype()
    {
        return $this->aircrafttype;
    }

    /**
     * @return the $datetime
     */
    public function getDatetime()
    {
        $dateFormat=explode("T", $this->datetime);
        $dateFormat=explode(":",$dateFormat[1]);
        return $dateFormat[0].":".$dateFormat[1];
    }

    /**
     * @return the $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return the $seatsAvailable
     */
    public function getSeatsAvailable()
    {
        return $this->seatsAvailable;
    }

    /**
     * @return the $departCode
     */
    public function getDepartCode()
    {
        return $this->departCode;
    }

    /**
     * @return the $departName
     */
    public function getDepartName()
    {
        return $this->departName;
    }

    /**
     * @return the $arrivalCode
     */
    public function getArrivalCode()
    {
        return $this->arrivalCode;
    }

    /**
     * @return the $arrivalName
     */
    public function getArrivalName()
    {
        return $this->arrivalName;
    }

    /**
     * @param field_type $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param field_type $aircrafttype
     */
    public function setAircrafttype($aircrafttype)
    {
        $this->aircrafttype = $aircrafttype;
    }

    /**
     * @param field_type $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * @param field_type $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param field_type $seatsAvailable
     */
    public function setSeatsAvailable($seatsAvailable)
    {
        $this->seatsAvailable = $seatsAvailable;
    }

    /**
     * @param field_type $departCode
     */
    public function setDepartCode($departCode)
    {
        $this->departCode = $departCode;
    }

    /**
     * @param field_type $departName
     */
    public function setDepartName($departName)
    {
        $this->departName = $departName;
    }

    /**
     * @param field_type $arrivalCode
     */
    public function setArrivalCode($arrivalCode)
    {
        $this->arrivalCode = $arrivalCode;
    }

    /**
     * @param field_type $arrivalName
     */
    public function setArrivalName($arrivalName)
    {
        $this->arrivalName = $arrivalName;
    }
    /**
     * @return the $esPrecioMinimo
     */
    public function getEsPrecioMinimo()
    {
        return $this->esPrecioMinimo;
    }

    /**
     * @param boolean $esPrecioMinimo
     */
    public function setEsPrecioMinimo($esPrecioMinimo)
    {
        $this->esPrecioMinimo = $esPrecioMinimo;
    }
    /**
     * @return the $adults
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @return the $children
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param field_type $adults
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;
    }

    /**
     * @param field_type $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }


    
    
    
}