<?php

namespace Vuelos\Model;

class EntidadVuelos{
    private $departure;
    private $destination;
    private $fsalida;
    private $fvuelta;
    private $adultos;
    private $children;
    private $babies;
    
    public function __construct($post=array()){
        $this->departure=$post['departure'];
        $this->destination=$post['destination'];
        $this->fsalida=$post['fsalida'];
        $this->fvuelta=$post['fvuelta'];
        $this->adultos=$post['adultos'];
        $this->children=$post['children'];
        $this->babies=$post['babies'];
    }
    /**
     * @return the $departure
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * @return the $destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @return the $fsalida
     */
    public function getFsalida()
    {
        if (strlen($this->fsalida)>0){
            $fechaDMY=explode("/", $this->fsalida);
            return $fechaDMY[2].$fechaDMY[0].$fechaDMY[1];
        }
        return $this->fsalida;
    }

    /**
     * @return the $fvuelta
     */
    public function getFvuelta()
    {
        if (strlen($this->fvuelta)>0){
            $fechaDMY=explode("/", $this->fvuelta);
            return $fechaDMY[2].$fechaDMY[0].$fechaDMY[1];
        }
        return $this->fvuelta;
    }

    /**
     * @return the $adultos
     */
    public function getAdultos()
    {
        return $this->adultos;
    }

    /**
     * @return the $children
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return the $babies
     */
    public function getBabies()
    {
        return $this->babies;
    }

    /**
     * @param field_type $departure
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;
    }

    /**
     * @param field_type $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @param field_type $fsalida
     */
    public function setFsalida($fsalida)
    {
        $this->fsalida = $fsalida;
    }

    /**
     * @param field_type $fvuelta
     */
    public function setFvuelta($fvuelta)
    {
        $this->fvuelta = $fvuelta;
    }

    /**
     * @param field_type $adultos
     */
    public function setAdultos($adultos)
    {
        $this->adultos = $adultos;
    }

    /**
     * @param field_type $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @param field_type $babies
     */
    public function setBabies($babies)
    {
        $this->babies = $babies;
    }

    
    
}