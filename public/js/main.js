function verify(){

	if ($('#departure').val()==""){
		alert('You have to choose departure');
		return false;
	}
	
	if ($('#destination').val()==""){
		alert('You have to choose destination');
		return false;
	}
	
	if ($("input[name='fsalida']").val()==""){
		alert('You have to choose date of departure');
		return false;
	}
	
	if ($("input[name='fvuelta']").val()==""){
		alert('You have to choose date of return');
		return false;
	}
	return true;
}

function verifySelectedFlight(){
	if (!$('input[name=salida]').is(':checked')){
		alert('You have to choose a outbound flight');
		return false;
	}
	
	if (!$('input[name=vuelta]').is(':checked')){
		alert('You have to choose a return flight');
		return false;
	}
	return true;
}

function departure_changed(element){
	var departure=$(element).val();
	
	if (departure==''){
		$('#destination').find('option').remove();
		$('#destination').append($('<option>', { 
	        value: '',
	        text : 'To' 
	    }));
    	
	}else{
		$.ajax({
		    url: "/vuelos/cargardestination",
		    type: "POST",
		    dataType: 'json',
		    data: { departure: departure  },
		    success: function(data){
		    	$('#destination').find('option').remove();
		    	$('#destination').append($('<option>', { 
	    	        value: '',
	    	        text : 'To' 
	    	    }));
		    	
		    	$.each(data,function(index,value){
		    		$('#destination').append($('<option>', { 
		    	        value: index,
		    	        text : value 
		    	    }));
		    	});
		    },
		    error:function(){

		    }
		});
	}
}

function destination_changed(element){
	var destination=$(element).val();
	
	if (destination==''){
    	
	}else{
		$.ajax({
		    url: "/vuelos/cargarfechas",
		    type: "POST",
		    dataType: 'json',
		    data: { departure: $('#departure').val(),destination: destination },
		    success: function(data){
		    	
		    	availableDatesDeparture=data.salidas;
		    	//$( "input[name='fsalida']" ).datepicker({ beforeShowDay: availableDeparture });
		    	
		    	availableDatesDestination=data.llegadas;
		    	//$( "input[name='fvuelta']" ).datepicker({ beforeShowDay: availableDestination });
		    	
		    },
		    error:function(){

		    }
		});
	}
}

availableDatesDeparture = ["2017-5-9","19-5-2017","20-5-2017"];

function availableDeparture(date) {
	var month=date.getMonth()+1;
	if (month<10){
		month="0"+month+"";
	}
	var day=date.getDate();
	if (day<10){
		day="0"+day+"";
	}
	  dmy = date.getFullYear() + "-" + month + "-" + day;
  if ($.inArray(dmy, availableDatesDeparture) != -1) {
    return [true, "","Available"];
  } else {
    return [false,"","unAvailable"];
  }
}
availableDatesDestination = ["2017-5-9","19-5-2017","20-5-2017"];
function availableDestination(date) {
	var month=date.getMonth()+1;
	if (month<10){
		month="0"+month+"";
	}
	var day=date.getDate();
	if (day<10){
		day="0"+day+"";
	}
	  dmy = date.getFullYear() + "-" + month + "-" + day;
	  if ($.inArray(dmy, availableDatesDestination) != -1) {
	    return [true, "","Available"];
	  } else {
	    return [false,"","unAvailable"];
	  }
	}

$( function() {
    $( "input[name='fsalida']" ).datepicker({ beforeShowDay: availableDeparture });
} );

$( function() {
    $( "input[name='fvuelta']" ).datepicker({ beforeShowDay: availableDestination });
} );